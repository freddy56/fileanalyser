# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

This repository is for the FileAnalyser project which reads csv file and converts it to Person object and then ouputs two files of data from the 
csv file orded by Name Frequency and Address.

### How do I get set up? ###

Project created using VS 2017 Community edition
Project Type: .Net core console application

Configurations:
Console application is using JSon configuration file to get the csv file path and to know which location to write the output files to:
appsettings.json

Steps:
1. Copy data.csv file to c:\ or any location on you machine and then update the JSon configuration file.

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin: 
Freddy Kgapola
email: kgapza@gmail.com
* Other community or team contact