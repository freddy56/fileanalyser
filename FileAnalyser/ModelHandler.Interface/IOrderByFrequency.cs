﻿using Model;
using System;
using System.Collections.Generic;

namespace ModelHandler.Interface
{
    public interface IOrderByFrequency
    {
        IEnumerable<NameFrequency> OrderData(IEnumerable<Person> data);
    }
}
