﻿using Model;
using System;
using System.Collections.Generic;

namespace ModelHandler.Interface
{
    public interface IOrderByAddress
    {
        IEnumerable<Address> OrderData(IEnumerable<Person> data);
    }
}
