﻿using FileGenerator;
using Microsoft.Extensions.Configuration;
using Model;
using ModelHandler;
using Repository.TextFile;
using System;
using System.IO;

namespace FileAnalyser
{
    class Program
    {
        public static IConfigurationRoot Configuration { get; set; }
        static void Main(string[] args)
        {
            try
            {
                Console.WriteLine("Starting Program!");
                var builder = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
               .AddJsonFile("appsettings.json");

                Configuration = builder.Build();
                var configs = new FileConfiguration();
                Configuration.GetSection("personFile").Bind(configs);
                Console.WriteLine("App configurations");
                Console.WriteLine($"Person file Configuration: Field Delimeter={configs.FieldDelimiter} and File Full Path={configs.FileFullPath}");

                var frequencyFileConfigs = new FileConfiguration();
                Configuration.GetSection("frequencyFile").Bind(frequencyFileConfigs);
                Console.WriteLine($"Fequency file Configuration: Field Delimeter={frequencyFileConfigs.FieldDelimiter} and File Save Path={frequencyFileConfigs.SaveFilePath}");

                var addressFileConfigs = new FileConfiguration();
                Configuration.GetSection("addressFile").Bind(addressFileConfigs);
                Console.WriteLine($"Address file Configuration: Field Delimeter={addressFileConfigs.FieldDelimiter} and File Save Path={addressFileConfigs.SaveFilePath}");

                Console.WriteLine("Generate Address File");
                var addressGenerator = new AddressGenerator(new OrderByAddress(),
                    new PersonFileRepository(configs, new FileMapper<Person>()),
                    new AddressFileRepository(addressFileConfigs, new FileMapper<Address>()));
                Console.WriteLine($"Address file generated:{addressGenerator.GenerateFile()}");

                Console.WriteLine("Generate Address File");
                var frequencyGenerator = new FrequencyGenerator(new OrderByFrequency(),
                    new PersonFileRepository(configs, new FileMapper<Person>()),
                    new FrequencyFileRepository(frequencyFileConfigs, new FileMapper<NameFrequency>()));
                Console.WriteLine($"Address file generated:{frequencyGenerator.GenerateFile()}");
                Console.WriteLine("Press enter to exit");
                
            }catch(Exception ex)
            {
                Console.WriteLine($"Error:{ex.Message}");
            }
            Console.ReadLine();
        }
    }
}