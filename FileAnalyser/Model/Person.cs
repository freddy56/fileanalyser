﻿using System;

namespace Model
{
    public class Person
    {
        public string FirstName;
        public string LastName;
        public string Address;
        public string PhoneNumber;
    }
}
