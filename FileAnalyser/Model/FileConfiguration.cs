﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Model
{
    public class FileConfiguration
    {
        public string LineDelimiter { get; set; }
        public string FieldDelimiter { get; set; }
        public string FileFullPath { get; set; }
        public string SaveFilePath { get; set; }

        public FileConfiguration()
        {
            FieldDelimiter = ",";
            LineDelimiter = "\n";
            FileFullPath = @"c:\data.csv";
            SaveFilePath = @"c:\";
        }
    }
}
