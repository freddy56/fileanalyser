﻿using ModelHandler.Interface;
using Repository.TextFile.Interface;
using System;

namespace FileGenerator
{
    public class AddressGenerator
    {
        private IPersonFileRepository _IPersonFileRepository;
        private IOrderByAddress _IOrderyByAddress;
        private IAddressFileRepository _IAddressFileRepository;

        public AddressGenerator(IOrderByAddress iOrderyByAddress, IPersonFileRepository iPersonFileRepository, IAddressFileRepository iAddressFileRepository)
        {
            _IOrderyByAddress = iOrderyByAddress;
            _IAddressFileRepository = iAddressFileRepository;
            _IPersonFileRepository = iPersonFileRepository;
        }

        public bool GenerateFile()
        {
            var fileObjects = _IPersonFileRepository.GetLines();
            var formatedResults = _IOrderyByAddress.OrderData(fileObjects);
            return _IAddressFileRepository.CreateFile(formatedResults);
        }
    }
}
