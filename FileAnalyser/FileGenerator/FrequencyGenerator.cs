﻿using ModelHandler.Interface;
using Repository.TextFile.Interface;
using System;

namespace FileGenerator
{
    public class FrequencyGenerator
    {
        private IPersonFileRepository _IPersonFileRepository;
        private IOrderByFrequency _IOrderyByFrequency;
        private IFrequencyFileRepository _IFrequencyFileRepository;

        public FrequencyGenerator(IOrderByFrequency iOrderyByFrequency, IPersonFileRepository iPersonFileRepository, IFrequencyFileRepository iFrequencyFileRepository)
        {
            _IOrderyByFrequency = iOrderyByFrequency;
            _IFrequencyFileRepository = iFrequencyFileRepository;
            _IPersonFileRepository = iPersonFileRepository;
        }

        public bool GenerateFile()
        {
            var fileObjects = _IPersonFileRepository.GetLines();
            var formatedResults = _IOrderyByFrequency.OrderData(fileObjects);
            return _IFrequencyFileRepository.CreateFile(formatedResults);
        }
    }
}
