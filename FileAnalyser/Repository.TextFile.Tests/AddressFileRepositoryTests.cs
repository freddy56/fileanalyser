﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Model;
using Repository.TextFile.Interface;
using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Text;

namespace Repository.TextFile.Tests
{
    [TestClass]
    public class AddressFileRepositoryTests
    {
        private IAddressFileRepository _IAddressFileRepository;

        [TestInitialize]
        public void Initialize()
        {
            var assemblyName = new AssemblyName("Repository.TextFile.Tests");
            string path = System.IO.Path.Combine(Path.GetDirectoryName(Assembly.Load(assemblyName).Location), "data.csv");
            _IAddressFileRepository = new AddressFileRepository(new Model.FileConfiguration
            {
                FieldDelimiter = " ",
                LineDelimiter = "\n",
                FileFullPath = path,
                SaveFilePath = Path.GetDirectoryName(Assembly.Load(assemblyName).Location)
            }, new FileMapper<Address>());
        }

        [TestMethod]
        public void Test_CreateFile_WhenDataIspassed_ShldReturnWriteDataToFile()
        {
            var result = _IAddressFileRepository.CreateFile(
                new List<Address>() { new Address{ StreetName = "Acton St", StreetNumber=12 },
                new Address{ StreetNumber=31, StreetName="Clifton Rd"},
                new Address{ StreetNumber=22, StreetName="Jones Rd"} }
                );
            Assert.IsTrue(result);
        }
    }
}
