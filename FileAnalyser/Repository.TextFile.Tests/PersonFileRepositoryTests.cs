using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Repository.TextFile.Interface;
using Model;
using System.Reflection;
using System.IO;

namespace Repository.TextFile.Tests
{
    [TestClass]
    public class PersonFileRepositoryTests
    {
        private IPersonFileRepository _IPersonFileRepository;

        [TestInitialize]
        public void Initialize()
        {
            var assemblyName = new AssemblyName("Repository.TextFile.Tests");
            string path = System.IO.Path.Combine(Path.GetDirectoryName(Assembly.Load(assemblyName).Location), "data.csv");
            _IPersonFileRepository = new PersonFileRepository(new Model.FileConfiguration
            {
                FieldDelimiter = ",",
                LineDelimiter = "\n",
                FileFullPath = path,
                SaveFilePath = string.Empty
            },new FileMapper<Person>());
        }

        [TestMethod]
        public void Test_CreateFile_WhenDataIspassed_ShldReturnWriteDataToFile()
        {
            var results = _IPersonFileRepository.GetLines();
            Assert.IsTrue(results.Count == 8);
        }
    }
}
