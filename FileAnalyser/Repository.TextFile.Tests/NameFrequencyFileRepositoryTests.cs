﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Model;
using Repository.TextFile.Interface;
using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Text;

namespace Repository.TextFile.Tests
{
    [TestClass]
    public class NameFrequencyFileRepositoryTests
    {
        private IFrequencyFileRepository _IFrequencyFileRepository;

        [TestInitialize]
        public void Initialize()
        {
            var assemblyName = new AssemblyName("Repository.TextFile.Tests");
            string path = System.IO.Path.Combine(Path.GetDirectoryName(Assembly.Load(assemblyName).Location), "data.csv");
            _IFrequencyFileRepository = new FrequencyFileRepository(new Model.FileConfiguration
            {
                FieldDelimiter = ",",
                LineDelimiter = "\n",
                FileFullPath = path,
                SaveFilePath = Path.GetDirectoryName(Assembly.Load(assemblyName).Location)
            }, new FileMapper<NameFrequency>());
        }

        [TestMethod]
        public void Test_CreateFile_WhenDataIspassed_ShldReturnWriteDataToFile()
        {
            var result = _IFrequencyFileRepository.CreateFile(
                new List<NameFrequency>()
                {
                    new NameFrequency{ Name="Smith", Count=4  },
                    new NameFrequency{ Name="Jones", Count=3  }
                });
            Assert.IsTrue(result);
        }
    }
}
