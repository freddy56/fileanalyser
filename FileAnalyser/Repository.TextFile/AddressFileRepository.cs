﻿using System;
using Model;
using Repository.TextFile.Interface;
using System.Collections.Generic;

namespace Repository.TextFile
{
    public class AddressFileRepository : FileRepository<Address>, IAddressFileRepository
    {
        public AddressFileRepository(FileConfiguration fileConfigurations, IFileMapper<Address> fileMapper) 
            : base(fileConfigurations, fileMapper)
        {
        }

        public bool CreateFile(IEnumerable<Address> Addresses)
        {
            string fileName = $"address_{DateTime.Now.ToString("yyyyMMddHHmmssfff")}.txt";
            return base.CreateFile(fileName: fileName, filedataObjects: Addresses);
        }
    }
}
