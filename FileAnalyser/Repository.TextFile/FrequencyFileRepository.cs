﻿using System;
using Model;
using Repository.TextFile.Interface;
using System.Collections.Generic;

namespace Repository.TextFile
{
    public class FrequencyFileRepository : FileRepository<NameFrequency>, IFrequencyFileRepository
    {
        public FrequencyFileRepository(FileConfiguration fileConfigurations, IFileMapper<NameFrequency> fileMapper) 
            : base(fileConfigurations, fileMapper)
        {
        }


        public bool CreateFile(IEnumerable<NameFrequency> Names)
        {
            string fileName = $"namefreq_{DateTime.Now.ToString("yyyyMMddHHmmssfff")}.txt";
            return base.CreateFile(fileName: fileName, filedataObjects: Names);
        }
    }
}
