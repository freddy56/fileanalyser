﻿using Model;
using Repository.TextFile.Interface;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;

namespace Repository.TextFile
{
    public abstract class FileRepository<T> : IFileRepository<T>
    where T : class
    {
        private FileConfiguration _FileConfigurations;
        private IFileMapper<T> _FileMapper;

        public FileRepository(FileConfiguration fileConfigurations, IFileMapper<T> fileMapper)
        {
            _FileConfigurations = fileConfigurations;
            _FileMapper = fileMapper;
        }

        //public T FirstLine()
        //{
        //    var list = new List<T>();
        //    return list.First();
        //}

        //public T LastLine()
        //{
        //    var list = new List<T>();
        //    return list.First();
        //}

        //public IEnumerable<T> AllLines()
        //{
        //    return new List<T>();
        //}

        public List<T> GetLines()
        {
            string r = File.ReadAllText(_FileConfigurations.FileFullPath, Encoding.ASCII);
            string[] separator = { _FileConfigurations.LineDelimiter };
            var lines = r.Split(separator, StringSplitOptions.RemoveEmptyEntries);
            List<T> fileLines = new List<T>();
            //loop through all lines
            //if line cn be split 
            //fields first line
            string[] fields = GetFields(lines[0], _FileConfigurations.FieldDelimiter);
            foreach (var line in lines.Skip(1))
            {
                if (line.Contains(_FileConfigurations.FieldDelimiter))
                {
                    //map line to an object
                    fileLines.Add(_FileMapper.Map(Activator.CreateInstance<T>(),
                        GetFields(line, _FileConfigurations.FieldDelimiter),
                        fields));
                }
            }
            return fileLines;
        }

        public bool CreateFile(IEnumerable<T> filedataObjects, string fileName)
        {
            string fiedSeparator = _FileConfigurations.FieldDelimiter;
            CheckFolderExists();
            using (StreamWriter file = new StreamWriter(new FileStream(Path.Combine(Path.GetDirectoryName(_FileConfigurations.SaveFilePath),fileName), FileMode.CreateNew)))
            {
                foreach (var filedataObject in filedataObjects)
                    file.Write(ConvertObjectToFileString(fiedSeparator, filedataObject));
            }
            return true;
        }



        #region Private Methods

        private void CheckFolderExists()
        {
            bool exists = System.IO.Directory.Exists(_FileConfigurations.SaveFilePath);
            if (!exists)
                System.IO.Directory.CreateDirectory(_FileConfigurations.SaveFilePath);
        }

        private string[] GetFields(string fields, string delimiter)
        {
            return fields.Split(new string[] { delimiter },
                StringSplitOptions.None);
        }

        private string ConvertObjectToFileString(string separator, T fileDataObject)
        {
            var fields = typeof(T).GetFields();
            StringBuilder filedata = new StringBuilder();
            filedata.AppendLine(ConvertObjectToLineString(separator, fields, fileDataObject));
            return filedata.ToString();
        }

        private string ConvertObjectToLineString(string separator, IEnumerable<FieldInfo> fields, object fileDataObject)
        {
            StringBuilder line = new StringBuilder();
            foreach (var field in fields)
            {
                if (line.Length > 0)
                    line.Append(separator);
                var fieldValue = field.GetValue(fileDataObject);
                if (fieldValue != null)
                    line.Append(fieldValue.ToString());
            }
            return line.ToString();
        }

        #endregion

    }
}
