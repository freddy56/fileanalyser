﻿using Repository.TextFile.Interface;
using System;
using System.Linq;
using System.Reflection;

namespace Repository.TextFile
{
        public class FileMapper<T> : IFileMapper<T>
        where T : class
        {

            public FileMapper()
            {
            }

            public T Map(T item, string[] fieldVales, string[] fieldNames)
            {
                string fieldName = string.Empty;
                for (int i = 0; i < fieldVales.Count(); i++)
                {
                    if (TryIndex(fieldNames, i, out fieldName))
                    {
                        if (!string.IsNullOrEmpty(fieldName))
                        {
                            var field = item.GetType().GetField(fieldName);
                            if (field.FieldType == typeof(int))
                            {
                                if (int.TryParse(fieldVales[i], out int number))
                                    field.SetValue(item, number);
                            }
                            else
                                field.SetValue(item, fieldVales[i]);
                        }
                    }
                }
                return item;
            }

            #region Private Methods

            private bool TryIndex(string[] fieldNames, int index, out string result)
            {
                index = Math.Abs(index);
                bool success = false;
                result = string.Empty;
                if (fieldNames != null && index < fieldNames.Length)
                {
                    result = (string)fieldNames.GetValue(index);
                    success = true;
                }

                return success;
            }

            #endregion
        }
    }
