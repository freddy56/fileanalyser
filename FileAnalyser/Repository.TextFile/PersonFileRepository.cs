﻿using System;
using Model;
using Repository.TextFile.Interface;
using System.Collections.Generic;

namespace Repository.TextFile
{
    public class PersonFileRepository : FileRepository<Person>, IPersonFileRepository
    {
        public PersonFileRepository(FileConfiguration fileConfigurations, IFileMapper<Person> fileMapper) 
            : base(fileConfigurations, fileMapper)
        {
        }
    }
}
