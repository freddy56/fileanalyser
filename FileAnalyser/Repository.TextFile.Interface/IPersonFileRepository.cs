﻿using Model;

namespace Repository.TextFile.Interface
{
    public interface IPersonFileRepository : IFileRepository<Person>
    {
    }
}
