﻿using System;
using System.Collections.Generic;

namespace Repository.TextFile.Interface
{
    public interface IFileRepository<T>
    where T : class
    {

        /// <summary>
        /// Get Lines from a text file converted into an object
        /// </summary>
        /// <param name="fileName">name of the file</param>
        /// <returns>a collection of T object</returns>
        List<T> GetLines();
    }
}
