﻿using Model;
using System.Collections.Generic;

namespace Repository.TextFile.Interface
{
    public interface IAddressFileRepository : IFileRepository<Address>
    {
        bool CreateFile(IEnumerable<Address> Addresses);
    }
}
