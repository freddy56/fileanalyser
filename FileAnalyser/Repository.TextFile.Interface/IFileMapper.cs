﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Repository.TextFile.Interface
{
    public interface IFileMapper<T>
        where T : class
    {
        T Map(T item, string[] fields, string[] fieldNames);
    }
}
