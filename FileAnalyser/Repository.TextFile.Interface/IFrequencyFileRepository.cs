﻿using Model;
using System.Collections.Generic;

namespace Repository.TextFile.Interface
{
    public interface IFrequencyFileRepository : IFileRepository<NameFrequency>
    {
        bool CreateFile(IEnumerable<NameFrequency> Names);
    }
}
