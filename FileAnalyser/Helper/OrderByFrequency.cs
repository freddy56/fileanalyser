﻿using Model;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using ModelHandler.Interface;

namespace ModelHandler
{
    public class OrderByFrequency: IOrderByFrequency
    {
        public OrderByFrequency()
        {
        }

        public IEnumerable<NameFrequency> OrderData(IEnumerable<Person> data)
        {
            IList<NameFrequency> orderedData = new List<NameFrequency>();
            if (data == null)
                return orderedData;
            else
            {
                List<string> names = data.Select(c => c.FirstName).ToList();
                names.AddRange(data.Select(c => c.LastName));

                foreach (string name in names.Distinct())
                {
                    orderedData.Add(new NameFrequency
                    {
                        Name = name,
                        Count = names.Count(c => c == name)
                    });
                }
                return orderedData.OrderBy(c=>c.Name).OrderByDescending(c=>c.Count);
            }
        }
    }
}
