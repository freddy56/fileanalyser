﻿using Model;
using ModelHandler.Interface;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ModelHandler
{
    public class OrderByAddress : IOrderByAddress
    {
        public OrderByAddress()
        {
        }

        public IEnumerable<Address> OrderData(IEnumerable<Person> data)
        {
            if (data == null)
                return new List<Address>();
            else
            {
                IEnumerable<string> addresses = data.Select(c => c.Address);
                return addresses.Select(c =>
                {
                    string[] separator = { " " };
                    string[] addressDetails = c.Split(separator, StringSplitOptions.RemoveEmptyEntries);
                    if (int.TryParse(addressDetails[0], out int streetNumber))
                        return new Address { StreetNumber = streetNumber, StreetName = string.Join(" ", addressDetails.Skip(1)) };
                    else
                        return new Address { StreetNumber = 0, StreetName = c };
                }).OrderBy(c => c.StreetName);
            }
        }
    }
}
