﻿using Model;
using System;
using System.Collections.Generic;
using System.Text;

namespace ModelHandler.Tests
{
    public static class MockData
    {
        private static IEnumerable<Person> persons = new List<Person>
        {
                new Person{
                  FirstName ="Johnson",
                  LastName="Smith",
                  Address="12 Clifton Rd",
                   PhoneNumber="2393814226"
                 },
                new Person{
                  FirstName ="Heinrich",
                  LastName="Johnson",
                  Address=" 23 Jones Rd",
                   PhoneNumber="2393834226"
                 },
                 new Person{
                  FirstName ="Matt",
                  LastName="Brown",
                  Address="12 Acton St",
                   PhoneNumber="2393823456"
                 }
    };

        private static IEnumerable<Person> incorrectlyFormattedPersons = new List<Person>
        {
                new Person{
                  FirstName ="Johnson",
                  LastName="Smith",
                  Address="12Clifton Rd",
                   PhoneNumber="2393814226"
                 },
                new Person{
                  FirstName ="Heinrich",
                  LastName="Johnson",
                  Address=" 23 Jones Rd",
                   PhoneNumber="2393834226"
                 },
                 new Person{
                  FirstName ="Matt",
                  LastName="Brown",
                  Address="Acton St",
                   PhoneNumber="2393823456"
                 }
    };

        public static IEnumerable<Person> Persons { get => persons;  }

        public static IEnumerable<Person> IncorrectlyFormattedPersons { get => incorrectlyFormattedPersons; }
    }
}
