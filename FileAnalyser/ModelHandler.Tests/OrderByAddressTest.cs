using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Model;
using System.Collections.Generic;

namespace ModelHandler.Tests
{
    [TestClass]
    public class OrderByAddressTest
    {
        [TestMethod]
        public void Test_OrderData_WhenDataIsNotEmpty_ShldOrderByAddress()
        {
            

            var order = new OrderByAddress();
            var result = order.OrderData(MockData.Persons);
            Assert.IsTrue(result.First().StreetName.Equals("Acton St"));
            Assert.IsTrue(result.Last().StreetName.Equals("Jones Rd"));
        }

        [TestMethod]
        public void Test_OrderData_WhenDataIsNull_ShldReturnEmptyList()
        {
            var order = new OrderByAddress();
            var result = order.OrderData(null);
            Assert.IsTrue(result.Count()==0);
        }

        [TestMethod]
        public void Test_OrderData_WhenDataIsEmpty_ShldReturnEmptyList()
        {
            var order = new OrderByAddress();
            var result = order.OrderData(new List<Person>());
            Assert.IsTrue(result.Count() == 0);
        }

        [TestMethod]
        public void Test_OrderData_WhenDataIsNotEmptybtIncorrectlyFormatted_ShldOrderByAddress()
        {
            var order = new OrderByAddress();
            var result = order.OrderData(MockData.IncorrectlyFormattedPersons);
            Assert.IsTrue(result.First().StreetName.Equals("12Clifton Rd"));
            Assert.IsTrue(result.Last().StreetName.Equals("Jones Rd"));
        }
    }
}
