﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Model;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ModelHandler.Tests
{
    [TestClass]
    public class OrderByFrequencyTest
    {
        [TestMethod]
        public void Test_OrderData_WhenDataIsNull_ShldReturnEmptyList()
        {
            var orderData = new OrderByFrequency();
            var result = orderData.OrderData(null);
            Assert.IsTrue(result.Count() == 0);
        }

        [TestMethod]
        public void Test_OrderData_WhenDataIsEmptyList_ShldReturnEmptyList()
        {
            var orderData = new OrderByFrequency();
            var result = orderData.OrderData(new List<Person>());
            Assert.IsTrue(result.Count() == 0);
        }

        [TestMethod]
        public void Test_OrderData_WhenDataIsNotEmpty_SHldOrderDataByFrequency()
        {
            var orderData = new OrderByFrequency();
            var result = orderData.OrderData(MockData.Persons);
            Assert.IsTrue(result.Count() == 5);
            Assert.IsTrue(result.First().Name.Equals("Johnson"));
            Assert.IsTrue(result.First().Count==2);
        }
    }
}
